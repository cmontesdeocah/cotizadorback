const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const ObjectId = Schema.ObjectId;
let RuleSchema = new Schema({
    plazo_ini:Number,
    plazo_fin:Number,
    monto_ini:Number,
    monto_fin:Number,
    key:String
});
RuleSchema.pre('update', function (next, done) {
    this.dateModified = Date.now();
    next();
});
RuleSchema.pre('save', function (next, done) {
    this.dateModified = Date.now();
    next();
});
RuleSchema.methods.toJSON = function () {
    let obj = this.toObject();
    delete obj.__v;
    return obj
};
module.exports.RuleModel = mongoose.model('Rule',RuleSchema, 'rules');