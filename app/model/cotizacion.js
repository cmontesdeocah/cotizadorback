const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const ObjectId = Schema.ObjectId;
const autoIncrement = require('mongoose-auto-increment');
let CotizacionSchema = new Schema({
    IDSOL: String,
    codPersona: String,
    documento:{
        tipoDocumento:String,
        numDocumento:String
    },
    nomPersona:String,
    producto: {
        tipoProducto: String,
        nomProducto: String
    },
    modalidad: String,
    importeOperacion: {
        moneda: String,
        monto: String
    },
    garantia: String,
    plazo: {
        tipoPeriodo: String,
        periodo: String
    }, 
    teaSugerida: String,
    teaMinima: String,
    teaSolicitada: String,
    comentarios: String,
    estado: String,        
    dateCreated: {type: Date, default: Date.now},
    dateModified: {type: Date, default: Date.now},
});
autoIncrement.initialize(mongoose.connection);
CotizacionSchema.plugin(autoIncrement.plugin, { model: 'Cotizacion', field: 'IDSOL' });
CotizacionSchema.pre('update', function (next, done) {
    this.dateModified = Date.now();
    next();
});
CotizacionSchema.pre('save', function (next, done) {
    this.dateModified = Date.now();
    next();
});
CotizacionSchema.methods.toJSON = function () {
    let obj = this.toObject();
    delete obj.__v;
    return obj
};

module.exports.CotizacionModel = mongoose.model('Cotizacion', CotizacionSchema, 'cotizaciones');