const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const ObjectId = Schema.ObjectId;
let PersonaSchema = new Schema({
    codCentral: String,
    documento: {
        tipoDocumento: String,
        numDocumento: String
    },
    nombre: String,
    segmento: {
        codSegmento: String,
        nomSegmento: String
    },
    buro: String,
    nivelRiesgo: String,
    oficina: {
        codOficina: String,
        nomOficina: String
    }, 
    gestor: {
        codGestor: String,
        nomGestor: String
    },
    vinculacion: String, 
    fecAlta: String,       
    dateCreated: {type: Date, default: Date.now},
    dateModified: {type: Date, default: Date.now},
});
PersonaSchema.pre('update', function (next, done) {
    this.dateModified = Date.now();
    next();
});
PersonaSchema.pre('save', function (next, done) {
    this.dateModified = Date.now();
    next();
});
PersonaSchema.methods.toJSON = function () {
    let obj = this.toObject();
    delete obj.__v;
    return obj
};
module.exports.PersonaModel = mongoose.model('Persona', PersonaSchema, 'personas');