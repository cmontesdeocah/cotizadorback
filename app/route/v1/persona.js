const router = require('express').Router();
const PersonaController = require(APP_CONTROLLER_PATH + 'persona');
let personaController = new PersonaController();

router.get('/', personaController.getAll);
router.get('/:id', personaController.get);
router.post('/', personaController.create);
router.delete('/:id', personaController.remove);
router.put('/:id', personaController.update);
router.get('/:codCentral', personaController.getPersonByDocument);
router.get('/tipoDocumento/:tipoDocumento/numDocumento/:numDocumento', personaController.getPersonByDocument);
module.exports = router;
