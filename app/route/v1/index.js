
const express = require('express'), router = express.Router();

const ROUTE_V1_PATH = APP_ROUTE_PATH + "v1/";
router.use('/auth', require(ROUTE_V1_PATH + 'auth'));
router.use('/users', require(ROUTE_V1_PATH + 'user'));
router.use('/personas', require(ROUTE_V1_PATH + 'persona'));
router.use('/cotizaciones', require(ROUTE_V1_PATH + 'cotizacion'));
router.use('/rules', require(ROUTE_V1_PATH + 'rule'));
module.exports = router;