const router = require('express').Router();
const CotizacionController = require(APP_CONTROLLER_PATH + 'cotizacion');
let cotizacionController = new CotizacionController();

router.get('/', cotizacionController.getAll);
router.get('/:id', cotizacionController.get);
router.post('/', cotizacionController.create);
router.delete('/:id', cotizacionController.remove);
router.delete('/idsol/:idsol', cotizacionController.removeIdSol);
router.put('/:id', cotizacionController.update);
router.get('/idsol/:idsol/tipoDocumento/:tipoDocumento/numDocumento/:numDocumento', cotizacionController.filterQuotations);

module.exports = router;
