const router = require('express').Router();
const RuleController = require(APP_CONTROLLER_PATH + 'rule');
let ruleController = new RuleController();

router.get('/', ruleController.getAll);
router.get('/plazo/:plazo/monto/:monto', ruleController.getRuleKey);
module.exports = router;