const CotizacionModel = require(APP_MODEL_PATH + 'cotizacion').CotizacionModel;
const ValidationError = require(APP_ERROR_PATH + 'validation');
const NotFoundError = require(APP_ERROR_PATH + 'not-found');
const BaseAutoBindedClass = require(APP_BASE_PACKAGE_PATH + 'base-autobind');

class CotizacionHandler extends BaseAutoBindedClass {
    constructor() {
        super();
        this._validator = require('validator');
    }

    static get COTIZACION_VALIDATION_SCHEME() {
        return {
            'codPersona': {
                notEmpty: true,
                isLength: {
                    options: [{min: 5, max: 20}],
                    errorMessage: 'El codPersona debe tener un valor entre 5 a 20 caracteres'
                },
                errorMessage: 'codPersona invalido'
            }
        };
    }

    createNewCotizacion(req, callback) {
        let data = req.body;
        let validator = this._validator;
        req.checkBody(CotizacionHandler.COTIZACION_VALIDATION_SCHEME);
        req.getValidationResult()
            .then(function (result) {
                if (!result.isEmpty()) {
                    let errorMessages = result.array().map(function (elem) {
                        return elem.msg;
                    });
                    throw new ValidationError('errores de validacion: ' + errorMessages.join(' && '));
                }
                return new CotizacionModel({
                    codPersona: data.codPersona,
                    documento:data.documento,
                    nomPersona:data.nomPersona,
                    producto: data.producto,
                    modalidad: data.modalidad,
                    importeOperacion: data.importeOperacion,
                    garantia: data.garantia,
                    plazo: data.plazo,
                    teaSugerida: data.teaSugerida,
                    teaMinima: data.teaMinima,
                    teaSolicitada: data.teaSolicitada,
                    comentarios: data.comentarios,
                    estado: data.estado,
                });
            })
            .then((cotizacion) => {
                cotizacion.save();
                return cotizacion;
            })
            .then((saved) => {
                callback.onSuccess(saved);
            })
            .catch((error) => {
                callback.onError(error);
            });
    }

    deleteCotizacion(req, callback) {
        let data = req.body;
        req.checkParams('id', 'Invalid post id provided').isMongoId();
        req.getValidationResult()
            .then(function (result) {
                    if (!result.isEmpty()) {
                        let errorMessages = result.array().map(function (elem) {
                            return elem.msg;
                        });
                        throw new ValidationError('There are validation errors: ' + errorMessages.join(' && '));
                    }
                    return new Promise(function (resolve, reject) {
                        CotizacionModel.findOne({_id: req.params.id}, function (err, cotizacion) {
                            if (err !== null) {
                                reject(err);
                            } else {
                                if (!cotizacion) {
                                    reject(new NotFoundError("Cotizacion no existe"));
                                }
                                else {
                                    resolve(cotizacion);
                                }
                            }
                        })
                    });
                }
            )
            .then((cotizacion) => {
                cotizacion.remove();
                return cotizacion;
            })
            .then((saved) => {
                callback.onSuccess(saved);
            })
            .catch((error) => {
                callback.onError(error);
            });
    }

    deleteCotizacionIdSol(req, callback) {
        console.log("Ingreso a deleteCotizacionIdSol.");
        return new Promise(function (resolve, reject) {
            console.log(req.params);
            CotizacionModel.findOne({IDSOL:req.params.idsol}, function (err, cotizacion) {
                if (err !== null) {
                    reject(err);
                } else {
                    if (!cotizacion) {
                        reject(new NotFoundError("Cotizacion no existe"));
                    }
                    else {
                        resolve(cotizacion);
                    }
                }
            })
        })
        .then((cotizacion) => {
            cotizacion.remove();
            return cotizacion;
        })
        .then((saved) => {
            callback.onSuccess(saved);
        })
        .catch((error) => {
            callback.onError(error);
        });
    }    

    updateCotizacion(req, callback) {
        let data = req.body;
        let validator = this._validator;
        req.checkBody(CotizacionHandler.COTIZACION_VALIDATION_SCHEME);
        req.getValidationResult()
            .then(function (result) {
                    if (!result.isEmpty()) {
                        let errorMessages = result.array().map(function (elem) {
                            return elem.msg;
                        });
                        throw new ValidationError('There are validation errors: ' + errorMessages.join(' && '));
                    }
                    return new Promise(function (resolve, reject) {
                        CotizacionModel.findOne({_id: req.params.id}, function (err, cotizacion) {
                            if (err !== null) {
                                reject(err);
                            } else {
                                if (!cotizacion) {
                                    reject(new NotFoundError("Cotizacion no existe"));
                                }
                                else {
                                    resolve(cotizacion);
                                }
                            }
                        })
                    });
                }
            )
            .then((cotizacion) => {
                cotizacion.codPersona = validator.trim(data.codPersona);
                cotizacion.producto = data.producto;
                cotizacion.modalidad = data.modalidad;
                cotizacion.importeOperacion = data.importeOperacion;
                cotizacion.garantia = data.garantia;
                cotizacion.plazo = data.plazo;
                cotizacion.teaSugerida = data.teaSugerida;
                cotizacion.teaMinima = data.teaMinima;
                cotizacion.teaSolicitada = data.teaSolicitada;
                cotizacion.comentarios = data.comentarios;
                cotizacion.estado = data.estado;
                cotizacion.save();
                return cotizacion;
            })
            .then((saved) => {
                callback.onSuccess(saved);
            })
            .catch((error) => {
                callback.onError(error);
            });
    }

    getSingleCotizacion(req, callback) {
        let data = req.body;
        req.checkParams('id', 'Invalid post id provided').isMongoId();
        req.getValidationResult()
            .then(function (result) {
                    if (!result.isEmpty()) {
                        let errorMessages = result.array().map(function (elem) {
                            return elem.msg;
                        });
                        throw new ValidationError('There are validation errors: ' + errorMessages.join(' && '));
                    }
                    return new Promise(function (resolve, reject) {
                        CotizacionModel.findOne({_id: req.params.id}, function (err, cotizacion) {
                            if (err !== null) {
                                reject(err);
                            } else {
                                if (!cotizacion) {
                                    reject(new NotFoundError("Cotizacion no existe"));
                                }
                                else {
                                    resolve(cotizacion);
                                }
                            }
                        })
                    });
                }
            )
            .then((cotizacion) => {
                callback.onSuccess(cotizacion);
            })
            .catch((error) => {
                callback.onError(error);
            });
    }

    getAllCotizaciones(req, callback) {
        let data = req.body;
        new Promise(function (resolve, reject) {
            CotizacionModel.find({}, function (err, cotizaciones) {
                if (err !== null) {
                    reject(err);
                } else {
                    resolve(cotizaciones);
                }
            });
        })
        .then((cotizaciones) => {
            callback.onSuccess(cotizaciones);
        })
        .catch((error) => {
            callback.onError(error);
        });
    }

    filterQuotations(req, callback) {
        console.log("Ingreso a filterQuotations.");
        let data = req.body;
        new Promise(function (resolve, reject) {
        console.log(req.params);
        var filter = Object;
        if(req.params.idsol!="0"){
            filter={IDSOL:req.params.idsol};
        }else{
            filter={documento:{tipoDocumento:req.params.tipoDocumento,numDocumento:req.params.numDocumento}};
        }
        console.log(filter);
        CotizacionModel.find(filter, function (err, cotizacion) {
        if (err !== null) {
            reject(err);
        } else {
                if (!cotizacion) {
                    reject(new NotFoundError("persona no existe"));
                }
                else {
                    resolve(cotizacion);
                }
            }
        })
        })
        .then((cotizacion) => {
            callback.onSuccess(cotizacion);
        })
        .catch((error) => {
            callback.onError(error);
        });
    }
}

module.exports = CotizacionHandler;