const RuleModel = require(APP_MODEL_PATH + 'rule').RuleModel;
const ValidationError = require(APP_ERROR_PATH + 'validation');
const NotFoundError = require(APP_ERROR_PATH + 'not-found');
const BaseAutoBindedClass = require(APP_BASE_PACKAGE_PATH + 'base-autobind');

class RuleHandler extends BaseAutoBindedClass {
    constructor() {
        super();
        this._validator = require('validator');
    }

    static get PERSONA_VALIDATION_SCHEME() {
        return {
            'nombre': {
                notEmpty: true,
                isLength: {
                    options: [{min: 10, max: 150}],
                    errorMessage: 'El nombre debe tener un valor entre 20 a 150 caracteres'
                },
                errorMessage: 'nombre invalido'
            }
        };
    }
    
    getAll(req, callback) {
        console.log("Ingreso RulesHandler getAll");
        let data = req.body;
        new Promise(function (resolve, reject) {
            RuleModel.find({}, function (err, rule) {
                if (err !== null) {
                    reject(err);
                } else {
                    resolve(rule);
                }
            });
        })
            .then((rule) => {
                callback.onSuccess(rule);
            })
            .catch((error) => {
                callback.onError(error);
            });
    }

    getRuleKey(req, callback) {
        let data = req.body;
        return new Promise(function (resolve, reject) {
        console.log(req.params);
        RuleModel.findOne({plazo_ini:{$lte: req.params.plazo},plazo_fin:{$gte:req.params.plazo},monto_ini:{$lte: req.params.monto},
                            monto_fin:{$gte:req.params.monto}}, function (err, rule) {
        if (err !== null) {
            reject(err);
        } else {
                    if (!rule) {
                        reject(new NotFoundError("persona no existe"));
                    }
                    else {
                        resolve(rule);
                    }
                }
        })
        })
            .then((rule) => {
                callback.onSuccess(rule);
            })
            .catch((error) => {
                callback.onError(error);
            });
    }
}

module.exports = RuleHandler;