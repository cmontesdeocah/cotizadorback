const PersonaModel = require(APP_MODEL_PATH + 'persona').PersonaModel;
const ValidationError = require(APP_ERROR_PATH + 'validation');
const NotFoundError = require(APP_ERROR_PATH + 'not-found');
const BaseAutoBindedClass = require(APP_BASE_PACKAGE_PATH + 'base-autobind');

class PersonaHandler extends BaseAutoBindedClass {
    constructor() {
        super();
        this._validator = require('validator');
    }

    static get PERSONA_VALIDATION_SCHEME() {
        return {
            'nombre': {
                notEmpty: true,
                isLength: {
                    options: [{min: 10, max: 150}],
                    errorMessage: 'El nombre debe tener un valor entre 20 a 150 caracteres'
                },
                errorMessage: 'nombre invalido'
            }
        };
    }

    createNewPersona(req, callback) {
        let data = req.body;
        let validator = this._validator;
        req.checkBody(PersonaHandler.PERSONA_VALIDATION_SCHEME);
        req.getValidationResult()
            .then(function (result) {
                if (!result.isEmpty()) {
                    let errorMessages = result.array().map(function (elem) {
                        return elem.msg;
                    });
                    throw new ValidationError('errores de validacion: ' + errorMessages.join(' && '));
                }
                return new PersonaModel({
                    codCentral: validator.trim(data.codCentral),
                    documento: data.documento,
                    nombre: validator.trim(data.nombre),
                    segmento: data.segmento,
                    buro: data.buro,
                    nivelRiesgo: data.nivelRiesgo,
                    oficina: data.oficina,
                    gestor: data.gestor,
                    vinculacion: data.vinculacion,
                    fecAlta : data.fecAlta,
                });
            })
            .then((persona) => {
                persona.save();
                return persona;
            })
            .then((saved) => {
                callback.onSuccess(saved);
            })
            .catch((error) => {
                callback.onError(error);
            });
    }

    deletePersona(req, callback) {
        let data = req.body;
        req.checkParams('id', 'Invalid post id provided').isMongoId();
        req.getValidationResult()
            .then(function (result) {
                    if (!result.isEmpty()) {
                        let errorMessages = result.array().map(function (elem) {
                            return elem.msg;
                        });
                        throw new ValidationError('There are validation errors: ' + errorMessages.join(' && '));
                    }
                    return new Promise(function (resolve, reject) {
                        PersonaModel.findOne({_id: req.params.id}, function (err, persona) {
                            if (err !== null) {
                                reject(err);
                            } else {
                                if (!persona) {
                                    reject(new NotFoundError("persona no existe"));
                                }
                                else {
                                    resolve(persona);
                                }
                            }
                        })
                    });
                }
            )
            .then((persona) => {
                persona.remove();
                return persona;
            })
            .then((saved) => {
                callback.onSuccess(saved);
            })
            .catch((error) => {
                callback.onError(error);
            });
    }

    updatePersona(req, callback) {
        let data = req.body;
        let validator = this._validator;
        req.checkBody(PersonaHandler.PERSONA_VALIDATION_SCHEME);
        req.getValidationResult()
            .then(function (result) {
                    if (!result.isEmpty()) {
                        let errorMessages = result.array().map(function (elem) {
                            return elem.msg;
                        });
                        throw new ValidationError('There are validation errors: ' + errorMessages.join(' && '));
                    }
                    return new Promise(function (resolve, reject) {
                        PersonaModel.findOne({_id: req.params.id}, function (err, persona) {
                            if (err !== null) {
                                reject(err);
                            } else {
                                if (!persona) {
                                    reject(new NotFoundError("persona no existe"));
                                }
                                else {
                                    resolve(persona);
                                }
                            }
                        })
                    });
                }
            )
            .then((persona) => {
                persona.codCentral = validator.trim(data.codCentral);
                persona.documento = data.documento;
                persona.nombre = validator.trim(data.nombre);
                persona.segmento = data.segmento;
                persona.buro = data.buro;
                persona.nivelRiesgo = data.nivelRiesgo;
                persona.oficina = data.oficina;
                persona.gestor = data.gestor;
                persona.vinculacion = data.vinculacion;
                persona.fecAlta = data.fecAlta;
                persona.save();
                return persona;
            })
            .then((saved) => {
                callback.onSuccess(saved);
            })
            .catch((error) => {
                callback.onError(error);
            });
    }

    getSinglePersona(req, callback) {
        let data = req.body;
        req.checkParams('id', 'Invalid post id provided').isMongoId();
        req.getValidationResult()
            .then(function (result) {
                    if (!result.isEmpty()) {
                        let errorMessages = result.array().map(function (elem) {
                            return elem.msg;
                        });
                        throw new ValidationError('There are validation errors: ' + errorMessages.join(' && '));
                    }
                    return new Promise(function (resolve, reject) {
                        PersonaModel.findOne({_id: req.params.id}, function (err, persona) {
                            if (err !== null) {
                                reject(err);
                            } else {
                                if (!persona) {
                                    reject(new NotFoundError("persona no existe"));
                                }
                                else {
                                    resolve(persona);
                                }
                            }
                        })
                    });
                }
            )
            .then((persona) => {
                callback.onSuccess(persona);
            })
            .catch((error) => {
                callback.onError(error);
            });
    }

    getAllPersonas(req, callback) {
        let data = req.body;
        new Promise(function (resolve, reject) {
            PersonaModel.find({}, function (err, personas) {
                if (err !== null) {
                    reject(err);
                } else {
                    resolve(personas);
                }
            });
        })
            .then((personas) => {
                callback.onSuccess(personas);
            })
            .catch((error) => {
                callback.onError(error);
            });
    }

    
    getPersonByDocument(req, callback) {
 
        let data = req.body;
            return new Promise(function (resolve, reject) {
                console.log(req.params);
                PersonaModel.findOne({documento:{tipoDocumento:req.params.tipoDocumento,numDocumento:req.params.numDocumento}}, function (err, persona) {
                    if (err !== null) {
                        reject(err);
                    } else {
                        if (!persona) {
                            reject(new NotFoundError("persona no existe"));
                        }
                        else {
                            resolve(persona);
                        }
                    }
                })
            })
        .then((persona) => {
            callback.onSuccess(persona);
        })
        .catch((error) => {
            callback.onError(error);
        });
    }
}

module.exports = PersonaHandler;