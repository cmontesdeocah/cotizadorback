
const BaseController = require(APP_CONTROLLER_PATH + 'base');
const RuleHandler = require(APP_HANDLER_PATH + 'rule');
class RuleController extends BaseController {
    constructor() {
        super();
        this._RuleHandler = new RuleHandler();
        this._passport = require('passport');
    }

    getAll(req, res, next) {
        console.log("Ingreso RuleController getAll");
        this.authenticate(req, res, next, (token, user) => {
            this._RuleHandler.getAll(req, this._responseManager.getDefaultResponseHandler(res));
        });
    }

    authenticate(req, res, next, callback) {
        let responseManager = this._responseManager;
        this._passport.authenticate('jwt-rs-auth', {
            onVerified: callback,
            onFailure: function (error) {
                responseManager.respondWithError(res, error.status || 401, error.message);
            }
        })(req, res, next);
    }

    getRuleKey(req, res, next) {
        let responseManager = this._responseManager;
        this.authenticate(req, res, next, (token, user) => {
            this._RuleHandler.getRuleKey(req, responseManager.getDefaultResponseHandlerError(res, ((data, message, code) => {
                let hateosLinks = [responseManager.generateHATEOASLink(req.baseUrl, "GET", "collection")];
                responseManager.respondWithSuccess(res, code || responseManager.HTTP_STATUS.OK, data, message, hateosLinks);
            })));
        });
    }
}

module.exports = RuleController;