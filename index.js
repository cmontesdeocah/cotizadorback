
// Root path
global.APP_ROOT_PATH = __dirname + '/app/';
// Setear otros path
require('./config/global-paths');
// Setear configuracion
global.config = require('./config');

// Crear una Express App
const express = require('express');
const app = express();

// Dependencias
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const routes = require(APP_ROUTE_PATH);
const ValidationManager = require(APP_MANAGER_PATH + 'validation');
const authManager = require(APP_MANAGER_PATH + 'auth');
const validationManager = new ValidationManager();

// Conneccion BD
mongoose.Promise = global.Promise;

//Eliminar mensajes de advertencias
mongoose.set('useNewUrlParser', true);
mongoose.set('useFindAndModify', false);
mongoose.set('useCreateIndex', true);

mongoose.connect(config.db.MONGO_CONNECT_URL);

// middleware y JSON Formatter
app.use(bodyParser.json());
app.use(authManager.providePassport().initialize());

// Soluciona problema CORS
app.use(function(req, res, next){
    res.header("Access-Control-Allow-Origin","*");
    res.header("Access-Control-Allow-Methods", "GET, PUT, POST, DELETE");
    res.header("Access-Control-Allow-Headers","Origin, X-Requested-With, Content-Type, Accept, Authorization");
    next();
});

// validacion middleware
app.use(validationManager.provideDefaultValidator());

// routes
app.use('/', routes);

app.listen(global.config.server.PORT, function () {
    console.log('App Cotizador Back is running on ' + global.config.server.PORT);
});
