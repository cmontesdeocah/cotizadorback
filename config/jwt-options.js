
module.exports = {
    issuer: "api@bbva.com",
    audience: "api.bbva.com.pe",
    algorithm: "RS256"
};